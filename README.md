# OpenWeather -> PostgreSQL

Stores weather data from OpenWeather in a PostgreSQL database.

## Parameters

* `-u, --postgres-url <POSTGRES_URL>` - URI to postgres database (example: `postgres://user:password@host:port/database`)
* `--ca-path` - Optional path to CA certificate if using a private CA for the database connection
* `-k, --api-key <API_KEY>` - OpenWeather API key
* `--lat <LATITUDE>` - Latitude to use for OpenWeather API
* `--lon <LONGITUDE>` - Longitude to use for OpenWeather API
* `-r, --query-rate <QUERY_RATE>` - Rate in seconds to query the OpenWeather `One Call API` [default: 300]
* `-l, --language <LANGUAGE>` - Language of data returned from OpenWeather API. Reference: https://openweathermap.org/api/one-call-api#multi [default: en]
* `--units <UNITS>` - Units to use for OpenWeather API (`standard`, `metric`, `imperial`) [default: imperial]
* `--exclude-alerts` - Excludes "alerts" data from the resulting OpenWeather data
* `--exclude-current` - Excludes "current" data from the resulting OpenWeather data
* `--exclude-daily` - Excludes "daily" data from the resulting OpenWeather data
* `--exclude-hourly` - Excludes "hourly" data from the resulting OpenWeather data
* `--exclude-minutely` - Excludes "minutely" data from the resulting OpenWeather data
* `--debug` - Turn on additional debugging output

## Getting Started

Clone repository `git clone https://gitlab.com/mthollylab/openweather2postgres`

### Prerequisites

#### Rust programming language
Rust programming language is required. See https://www.rust-lang.org/tools/install to install the latest versions.

Test your version by running the following:

```bash
cargo version
rustc -V
```

#### Docker
If you want to build the Docker container, you must have Docker or an equivalent installed.
You can go here and find the community editions of Docker that you can install:

https://docs.docker.com/install/

#### OpenWeather API Key

Get your personal key here: https://openweathermap.org/api

This program calls the `One Call API` from OpenWeather. Information for that call can be found here: https://openweathermap.org/api/one-call-api

#### PostgreSQL

You have a couple of choices for how to install PostgreSQL:

* Install directly to your machine. Please follow the official installation documentation if you choose this route: https://www.postgresql.org/download/
* Run it as a docker container. Here is an example of how to do that:

```bash
docker pull postgres:13.1

docker run -d \
  -p 5432:5432 \
  -e POSTGRES_PASSWORD=YOUR_SECRET_ADMIN_PASSWORD \
  -e POSTGRES_DB=openweatherdb \
  -v $(PWD)/postgres-data:/var/lib/postgresql/data \
  postgres:13.1
```

__NOTE__: Make sure you replace the "YOUR_SECRED_ADMIN_PASSWORD" above.

More options for the `docker run` command above can be found here: https://hub.docker.com/_/postgres

To create the tables and views for this data, run the `openweather-postgres.sql` file. This will create 4 tables and 5 views:

__Tables__:
* `openweather_onecall_data`
* `openweather_units`
* `openweather_languages`
* `openweather_skycon_xref`

__NOTE__: The `units`, `languages` and `skycon_xref` tables are automatically populated with all appropriate values.

__Views__:
* `openweather_current_conditions`
* `openweather_minutely_forecast`
* `openweather_hourly_forecast`
* `openweather_daily_forecast`
* `openweather_alerts`

#### Grafana
If you want to visualise the data in a chart I can highly recommend [Grafana](https://grafana.com/). You can use the "READ_USER" credentials listed above for the data source connection in Grafana.

After you have setup Grafana and connected it to your PostgreSQL datasource, look in the [grafana](https://gitlab.com/mthollylab/openweather2postgres/grafana/) folder for a sample dashboard you can import to see data returned from the OpenWeather.com API.

## Author

__Dwayne Bradley__
* GitLab - [https://gitlab.com/dwaynebradley/](https://gitlab.com/dwaynebradley/)
* GitHub - [https://github.com/dwaynebradley](https://github.com/dwaynebradley/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

