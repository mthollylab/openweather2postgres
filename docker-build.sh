#!/bin/bash

# Make sure that `cross` is installed to make cross compiling easier
if ! command -v cross &> /dev/null
then
    echo "`cross` could not be found. Please install: https://github.com/rust-embedded/cross"
    exit
fi

if [ -z $1 ]
then
    export IMAGE_TAG=$(git branch --show-current)
else
    export IMAGE_TAG=$1
fi

# Do a relase build using cross and musl for a statically linked executable
cross build --release --target=x86_64-unknown-linux-musl

# Create the docker image - it will use the statically linked executable from above
docker build -t registry.gitlab.com/mthollylab/openweather2postgres:$IMAGE_TAG -f Dockerfile .
