FROM rust:1.69.0 AS build

FROM scratch
COPY ./target/x86_64-unknown-linux-musl/release/openweather2postgres /
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
USER 1000
ENTRYPOINT ["/openweather2postgres"]
