-- ============================================================
-- Remove any tables/views that already exist so we can recreate them
-- ============================================================
DROP VIEW IF EXISTS openweather_current_alerts;
DROP VIEW IF EXISTS openweather_current_conditions;
DROP VIEW IF EXISTS openweather_daily_forecast;
DROP VIEW IF EXISTS openweather_hourly_forecast;
DROP VIEW IF EXISTS openweather_minutely_forecast;

DROP TABLE IF EXISTS openweather_onecall_data;
DROP TABLE IF EXISTS openweather_languages;
DROP TABLE IF EXISTS openweather_units;
DROP TABLE IF EXISTS openweather_skycon_xref;


-- ============================================================
-- Create TABLES and populate relevant data
-- ============================================================

-- Create units table
CREATE TABLE openweather_units
(
	unit_id STRING PRIMARY KEY NOT NULL,
   temperature_unit STRING NOT NULL,
   temperature_unit_short STRING NOT NULL,
   wind_speed_unit STRING NOT NULL,
   wind_speed_unit_short STRING NOT NULL,
	description STRING
);

-- Insert values into the units table
INSERT INTO openweather_units
(unit_id, temperature_unit, temperature_unit_short, wind_speed_unit, wind_speed_unit_short, description)
VALUES
('standard','Kelvin','K','Meters/Sec','m/s','Temperature in Kelvin and wind speed in meters/sec'),
('metric','Celsius','C','Meters/Sec','m/s','Temperature in Celsius and wind speed in meters/sec'),
('imperial','Fahrenheit','F','Miles/Hr','mph','Temperature in Fahrenheit and wind speed in miles/hour');


-- Create language table
CREATE TABLE openweather_languages
(
	language_id STRING PRIMARY KEY NOT NULL,
	description STRING
);

-- Insert values into the language table
INSERT INTO openweather_languages
(language_id, description)
VALUES
('af','Afrikaans'),
('al','Albanian'),
('ar','Arabic'),
('az','Azerbaijani'),
('bg','Bulgarian'),
('ca','Catalan'),
('cz','Czech'),
('da','Danish'),
('de','German'),
('el','Greek'),
('en','English'),
('eu','Basque'),
('fa','Persian (Farsi)'),
('fi','Finnish'),
('fr','French'),
('gl','Galician'),
('he','Hebrew'),
('hi','Hindi'),
('hr','Croatian'),
('hu','Hungarian'),
('id','Indonesian'),
('it','Italian'),
('ja','Japanese'),
('kr','Korean'),
('la','Latvian'),
('lt','Lithuanian'),
('mk','Macedonian'),
('no','Norwegian'),
('nl','Dutch'),
('pl','Polish'),
('pt','Portuguese'),
('pt_br','Português Brasil'),
('ro','Romanian'),
('ru','Russian'),
('sv','Swedish'),
('se','Swedish'),
('sk','Slovak'),
('sl','Slovenian'),
('sp','Spanish'),
('es','Spanish'),
('sr','Serbian'),
('th','Thai'),
('tr','Turkish'),
('ua','Ukrainian'),
('uk','Ukrainian'),
('vi','Vietnamese'),
('zh_cn','Chinese Simplified'),
('zh_tw','Chinese Traditional'),
('zu','Zulu');


-- Create oncecall_data table
CREATE TABLE openweather_onecall_data
(
   onecall_uuid UUID PRIMARY KEY DEFAULT gen_random_uuid(),
   "timestamp" TIMESTAMP NOT NULL DEFAULT NOW(),
	latitude DOUBLE PRECISION NOT NULL DEFAULT 0.0,
	longitude DOUBLE PRECISION NOT NULL DEFAULT 0.0,
	language_id STRING NOT NULL REFERENCES openweather_languages (language_id),
	unit_id STRING NOT NULL REFERENCES openweather_units (unit_id),
	has_current_data BOOLEAN NOT NULL DEFAULT FALSE,
	has_minutely_data BOOLEAN NOT NULL DEFAULT FALSE,
	has_hourly_data BOOLEAN NOT NULL DEFAULT FALSE,
	has_daily_data BOOLEAN NOT NULL DEFAULT FALSE,
	has_alert_data BOOLEAN NOT NULL DEFAULT FALSE,
   weather_data JSONB NOT NULL
);

-- Create a descending index on the timestamp column so we can get to
-- the most recent values from OpenWeather
CREATE UNIQUE INDEX ON openweather_onecall_data ("timestamp" DESC);


-- Create the cross-reference table to determine the "skycon" (animated weather icon)
-- to use based on values from OpenWeather
CREATE TABLE openweather_skycon_xref
(
	condition_id SMALLINT,
	icon STRING NOT NULL,
	skycon STRING NOT NULL,
	PRIMARY KEY (condition_id, icon)
);

-- Insert the values into the cross reference table
INSERT INTO openweather_skycon_xref
(condition_id, icon, skycon)
VALUES
(200,'11d','THUNDER_SHOWERS_DAY'),
(200,'11n','THUNDER_SHOWERS_NIGHT'),
(201,'11d','THUNDER_RAIN'),
(201,'11n','THUNDER_RAIN'),
(202,'11d','THUNDER_RAIN'),
(202,'11n','THUNDER_RAIN'),
(210,'11d','THUNDER'),
(210,'11n','THUNDER'),
(211,'11d','THUNDER'),
(211,'11n','THUNDER'),
(212,'11d','THUNDER'),
(212,'11n','THUNDER'),
(221,'11d','THUNDER'),
(221,'11n','THUNDER'),
(230,'11d','THUNDER_SHOWERS_DAY'),
(230,'11n','THUNDER_SHOWERS_NIGHT'),
(231,'11d','THUNDER_RAIN'),
(231,'11n','THUNDER_RAIN'),
(232,'11d','THUNDER_RAIN'),
(232,'11n','THUNDER_RAIN'),
(300,'09d','RAIN'),
(300,'09n','RAIN'),
(301,'09d','RAIN'),
(301,'09n','RAIN'),
(302,'09d','RAIN'),
(302,'09n','RAIN'),
(310,'09d','RAIN'),
(310,'09n','RAIN'),
(311,'09d','RAIN'),
(311,'09n','RAIN'),
(312,'09d','RAIN'),
(312,'09n','RAIN'),
(313,'09d','RAIN'),
(313,'09n','RAIN'),
(314,'09d','RAIN'),
(314,'09n','RAIN'),
(321,'09d','RAIN'),
(321,'09n','RAIN'),
(500,'10d','SHOWERS_DAY'),
(500,'10n','SHOWERS_NIGHT'),
(501,'10d','SHOWERS_DAY'),
(501,'10n','SHOWERS_NIGHT'),
(502,'10d','RAIN'),
(502,'10n','RAIN'),
(503,'10d','RAIN'),
(503,'10n','RAIN'),
(504,'10d','RAIN'),
(504,'10n','RAIN'),
(511,'13d','HAIL'),
(511,'13n','HAIL'),
(520,'09d','RAIN'),
(520,'09n','RAIN'),
(521,'09d','RAIN'),
(521,'09n','RAIN'),
(522,'09d','RAIN'),
(522,'09n','RAIN'),
(531,'09d','RAIN'),
(531,'09n','RAIN'),
(600,'13d','SNOW_SHOWERS_DAY'),
(600,'13n','SNOW_SHOWERS_NIGHT'),
(601,'13d','SNOW'),
(601,'13n','SNOW'),
(602,'13d','SNOW'),
(602,'13n','SNOW'),
(611,'13d','SLEET'),
(611,'13n','SLEET'),
(612,'13d','SLEET'),
(612,'13n','SLEET'),
(613,'13d','SLEET'),
(613,'13n','SLEET'),
(615,'13d','RAIN_SNOW_SHOWERS_DAY'),
(615,'13n','RAIN_SNOW_SHOWERS_NIGHT'),
(616,'13d','RAIN_SNOW_SHOWERS_DAY'),
(616,'13n','RAIN_SNOW_SHOWERS_NIGHT'),
(620,'13d','RAIN_SNOW'),
(620,'13n','RAIN_SNOW'),
(621,'13d','RAIN_SNOW'),
(621,'13n','RAIN_SNOW'),
(622,'13d','RAIN_SNOW'),
(622,'13n','RAIN_SNOW'),
(701,'50d','FOG'),
(701,'50n','FOG'),
(711,'50d','FOG'),
(711,'50n','FOG'),
(721,'50d','FOG'),
(721,'50n','FOG'),
(731,'50d','WIND'),
(731,'50n','WIND'),
(741,'50d','FOG'),
(741,'50n','FOG'),
(751,'50d','FOG'),
(751,'50n','FOG'),
(761,'50d','FOG'),
(761,'50n','FOG'),
(762,'50d','FOG'),
(762,'50n','FOG'),
(771,'50d','WIND'),
(771,'50n','WIND'),
(781,'50d','WIND'),
(781,'50n','WIND'),
(800,'01d','CLEAR_DAY'),
(800,'01n','CLEAR_NIGHT'),
(801,'02d','PARTLY_CLOUDY_DAY'),
(801,'02n','PARTLY_CLOUDY_NIGHT'),
(802,'03d','PARTLY_CLOUDY_DAY'),
(802,'03n','PARTLY_CLOUDY_NIGHT'),
(803,'04d','PARTLY_CLOUDY_DAY'),
(803,'04n','PARTLY_CLOUDY_NIGHT'),
(804,'04d','CLOUDY'),
(804,'04n','CLOUDY');


-- ============================================================
-- Create VIEWS
-- ============================================================

-- View to display current conditions
CREATE OR REPLACE VIEW openweather_current_conditions AS
SELECT
    current.current_dt_utc,
    current.unit_id,
	 current.sunrise_dt_utc,
	 current.sunset_dt_utc,
	 current.temperature,
	 current.feels_like,
	 current.pressure,
	 current.humidity,
	 current.dew_point,
	 current.clouds,
	 current.uv_index,
	 current.visibility,
	 current.wind_speed,
	 current.wind_gust,
	 current.wind_deg,
	 current.rain_1h,
	 current.snow_1h,
	 current.weather_condition_id,
	 current.weather_name,
	 current.weather_description,
	 current.weather_icon,
 	 sk.skycon
FROM
(
   SELECT
     (((weather_data -> 'current' -> 'dt')::STRING)::INT)::TIMESTAMP as current_dt_utc,
     unit_id,
     (((weather_data -> 'current' -> 'sunrise')::STRING)::INT)::TIMESTAMP as sunrise_dt_utc,
     (((weather_data -> 'current' -> 'sunset')::STRING)::INT)::TIMESTAMP as sunset_dt_utc,
     ((weather_data -> 'current' -> 'temp')::STRING)::FLOAT as temperature,
     ((weather_data -> 'current' -> 'feels_like')::STRING)::FLOAT as feels_like,
     ((weather_data -> 'current' -> 'pressure')::STRING)::FLOAT as pressure,
     ((weather_data -> 'current' -> 'humidity')::STRING)::FLOAT as humidity,
     ((weather_data -> 'current' -> 'dew_point')::STRING)::FLOAT as dew_point,
     ((weather_data -> 'current' -> 'clouds')::STRING)::FLOAT as clouds,
     ((weather_data -> 'current' -> 'uvi')::STRING)::FLOAT as uv_index,
     ((weather_data -> 'current' -> 'visibility')::STRING)::FLOAT as visibility,
     ((weather_data -> 'current' -> 'wind_speed')::STRING)::FLOAT as wind_speed,
     ((weather_data -> 'current' -> 'wind_gust')::STRING)::FLOAT as wind_gust,
     ((weather_data -> 'current' -> 'wind_deg')::STRING)::FLOAT as wind_deg,
     ((weather_data -> 'current' -> 'rain' -> '1h')::STRING)::FLOAT as rain_1h,
     ((weather_data -> 'current' -> 'snow' -> '1h')::STRING)::FLOAT as snow_1h,
     ((weather_data -> 'current' -> 'weather' -> 0 -> 'id')::STRING)::INT as weather_condition_id,
     (weather_data -> 'current' -> 'weather' -> 0 ->> 'main')::STRING as weather_name,
     (weather_data -> 'current' -> 'weather' -> 0 ->> 'description')::STRING as weather_description,
     (weather_data -> 'current' -> 'weather' -> 0 ->> 'icon')::STRING as weather_icon
   FROM 
     openweather_onecall_data
   ORDER BY timestamp DESC
) AS current,
   openweather_skycon_xref sk
WHERE
   current.weather_condition_id = sk.condition_id AND
   current.weather_icon = sk.icon
ORDER BY current_dt_utc DESC
;


-- Create a view to retrieve the most recent "minutely" forecast
CREATE OR REPLACE VIEW openweather_minutely_forecast AS
SELECT
  (((b.elements -> 'dt')::STRING)::INT)::TIMESTAMP AS forecast_dt_utc,
  b.unit_id,
  ((b.elements -> 'precipitation')::STRING)::FLOAT AS precipitation_volume_forecast
FROM
(
  SELECT
    unit_id,
    jsonb_array_elements(weather_data -> 'minutely') AS elements
  FROM
  (
    SELECT weather_data, unit_id
    FROM openweather_onecall_data
    ORDER BY timestamp DESC
    LIMIT 1
  ) a
) b
;


-- Create a view to retrieve the most recent "hourly" forecast
CREATE OR REPLACE VIEW openweather_hourly_forecast AS
SELECT
   hourly.forecast_dt_utc,
   hourly.unit_id,
   hourly.temperature,
   hourly.feels_like,
   hourly.pressure,
   hourly.humidity,
   hourly.dew_point,
   hourly.uv_index,
   hourly.clouds,
   hourly.visibility,
   hourly.wind_speed,
   hourly.wind_gust,
   hourly.wind_deg,
   hourly.precipitation_probability,
   hourly.rain_volume_forecast,
   hourly.snow_volume_forecast,
   hourly.weather_condition_id,
   hourly.weather_name,
   hourly.weather_description,
   hourly.weather_icon,
   sk.skycon
FROM
(
  SELECT
    (((elements -> 'dt')::STRING)::INT)::TIMESTAMP AS forecast_dt_utc,
    unit_id,
    ((elements -> 'temp')::STRING)::FLOAT AS temperature,
    ((elements -> 'feels_like')::STRING)::FLOAT AS feels_like,
    ((elements -> 'pressure')::STRING)::FLOAT AS pressure,
    ((elements -> 'humidity')::STRING)::FLOAT AS humidity,
    ((elements -> 'dew_point')::STRING)::FLOAT AS dew_point,
    ((elements -> 'uvi')::STRING)::FLOAT AS uv_index,
    ((elements -> 'clouds')::STRING)::FLOAT AS clouds,
    ((elements -> 'visibility')::STRING)::FLOAT AS visibility,
    ((elements -> 'wind_speed')::STRING)::FLOAT AS wind_speed,
    ((elements -> 'wind_gust')::STRING)::FLOAT AS wind_gust,
    ((elements -> 'wind_deg')::STRING)::FLOAT AS wind_deg,
    ((elements -> 'pop')::STRING)::FLOAT AS precipitation_probability,
    ((elements -> 'rain' -> '1h')::STRING)::FLOAT AS rain_volume_forecast,
    ((elements -> 'snow' -> '1h')::STRING)::FLOAT AS snow_volume_forecast,
    ((elements -> 'weather' -> 0 -> 'id')::STRING)::INT AS weather_condition_id,
    (elements -> 'weather' -> 0 ->> 'main')::STRING AS weather_name,
    (elements -> 'weather' -> 0 ->> 'description')::STRING AS weather_description,
    (elements -> 'weather' -> 0 ->> 'icon')::STRING AS weather_icon
   FROM
   (
      SELECT
         unit_id,
         jsonb_array_elements(weather_data -> 'hourly') AS elements
      FROM
      (
         SELECT weather_data, unit_id
         FROM openweather_onecall_data
         ORDER BY timestamp DESC
         LIMIT 1
      ) a
   ) b
) hourly,
	openweather_skycon_xref sk
WHERE
   hourly.weather_condition_id = sk.condition_id AND
   hourly.weather_icon = sk.icon
ORDER BY forecast_dt_utc ASC
;


-- Create a view to retrieve the most recent "daily" forecast
CREATE OR REPLACE VIEW openweather_daily_forecast AS
SELECT
   daily.forecast_dt_utc,
   daily.unit_id,
   daily.sunrise_dt_utc,
   daily.sunset_dt_utc,
   daily.temperature_morning,
   daily.temperature_day,
   daily.temperature_evening,
   daily.temperature_night,
   daily.temperature_min,
   daily.temperature_max,
   daily.feels_like_morning,
   daily.feels_like_day,
   daily.feels_like_evening,
   daily.feels_like_night,
   daily.pressure,
   daily.humidity,
   daily.dew_point,
   daily.wind_speed,
   daily.wind_gust,
   daily.wind_deg,
   daily.clouds,
   daily.uv_index,
   daily.precipitation_probability,
   daily.rain,
   daily.snow,
   daily.weather_condition_id,
   daily.weather_name,
   daily.weather_description,
   daily.weather_icon,
   sk.skycon
FROM
(
  SELECT
    (((elements -> 'dt')::STRING)::INT)::TIMESTAMP AS forecast_dt_utc,
    unit_id,
    (((elements -> 'sunrise')::string)::int)::timestamp as sunrise_dt_utc,
    (((elements -> 'sunset')::string)::int)::timestamp as sunset_dt_utc,
    ((elements -> 'temp' -> 'morn')::STRING)::FLOAT AS temperature_morning,
    ((elements -> 'temp' -> 'day')::STRING)::FLOAT AS temperature_day,
    ((elements -> 'temp' -> 'eve')::STRING)::FLOAT AS temperature_evening,
    ((elements -> 'temp' -> 'night')::STRING)::FLOAT AS temperature_night,
    ((elements -> 'temp' -> 'min')::STRING)::FLOAT AS temperature_min,
    ((elements -> 'temp' -> 'max')::STRING)::FLOAT AS temperature_max,
    ((elements -> 'feels_like' -> 'morn')::STRING)::FLOAT AS feels_like_morning,
    ((elements -> 'feels_like' -> 'day')::STRING)::FLOAT AS feels_like_day,
    ((elements -> 'feels_like' -> 'eve')::STRING)::FLOAT AS feels_like_evening,
    ((elements -> 'feels_like' -> 'night')::STRING)::FLOAT AS feels_like_night,
    ((elements -> 'pressure')::STRING)::FLOAT AS pressure,
    ((elements -> 'humidity')::STRING)::FLOAT AS humidity,
    ((elements -> 'dew_point')::STRING)::FLOAT AS dew_point,
    ((elements -> 'wind_speed')::STRING)::FLOAT AS wind_speed,
    ((elements -> 'wind_gust')::STRING)::FLOAT AS wind_gust,
    ((elements -> 'wind_deg')::STRING)::FLOAT AS wind_deg,
    ((elements -> 'clouds')::STRING)::FLOAT AS clouds,
    ((elements -> 'uvi')::STRING)::FLOAT AS uv_index,
    ((elements -> 'pop')::STRING)::FLOAT AS precipitation_probability,
    ((elements -> 'rain')::STRING)::FLOAT AS rain,
    ((elements -> 'snow')::STRING)::FLOAT AS snow,
    ((elements -> 'weather' -> 0 -> 'id')::STRING)::INT AS weather_condition_id,
    (elements -> 'weather' -> 0 ->> 'main')::STRING AS weather_name,
    (elements -> 'weather' -> 0 ->> 'description')::STRING AS weather_description,
    (elements -> 'weather' -> 0 ->> 'icon')::STRING AS weather_icon
   FROM
   (
      SELECT
         unit_id,
         jsonb_array_elements(weather_data -> 'daily') AS elements
      FROM
      (
         SELECT weather_data, unit_id
         FROM openweather_onecall_data
         ORDER BY timestamp DESC
         LIMIT 1
      ) a
   ) b
) daily,
  openweather_skycon_xref sk
WHERE
   daily.weather_condition_id = sk.condition_id AND
   daily.weather_icon = sk.icon
ORDER BY forecast_dt_utc ASC
;


-- Create a view to retrieve the current weather alerts
CREATE OR REPLACE VIEW openweather_current_alerts AS
SELECT
   (((elements -> 'start')::STRING)::INT)::TIMESTAMP AS start,
   (((elements -> 'end')::STRING)::INT)::TIMESTAMP AS end,
   (elements ->> 'event')::STRING AS event_name,
   (elements ->> 'sender_name')::STRING AS sender_name,
   (elements ->> 'description')::STRING AS description
FROM
(
   SELECT
      jsonb_array_elements(weather_data -> 'alerts') AS elements
   FROM
   (
      SELECT weather_data
      FROM openweather_onecall_data
      ORDER BY timestamp DESC
      LIMIT 1
   ) alerts
);
