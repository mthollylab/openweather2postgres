extern crate clap;
use clap::{Command, Arg};

use postgres::{Client, NoTls};
use openssl::ssl::{SslConnector, SslMethod};
use postgres_openssl::MakeTlsConnector;

extern crate timer;
extern crate chrono;

extern crate ctrlc;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::thread;
use std::time;

fn main() {
    let matches = Command::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .arg(
            Arg::new("API_KEY")
                .short('k')
                .long("api-key")
                .help("OpenWeather API key")
                .takes_value(true)
                .required(true)
        )
        .arg(
            Arg::new("LATITUDE")
                .long("lat")
                .help("Latitude to use for OpenWeather API")
                .takes_value(true)
                .required(true)
                .validator(validate_lat_long)
        )
        .arg(
            Arg::new("LONGITUDE")
                .long("lon")
                .help("Longitude to use for OpenWeather API")
                .takes_value(true)
                .required(true)
                .validator(validate_lat_long)
        )
        .arg(
            Arg::new("UNITS")
                .long("units")
                .help("Units to use for OpenWeather API")
                .takes_value(true)
                .required(false)
                .default_value("imperial")
                .possible_values(&["standard", "metric", "imperial"])
        )
        .arg(
            Arg::new("LANGUAGE")
                .short('l')
                .long("language")
                .help("Language of data returned from OpenWeather API. Reference: https://openweathermap.org/api/one-call-api#multi")
                .takes_value(true)
                .required(false)
                .default_value("en")
                .possible_values(&["af", "al", "ar", "az", "bg", "ca", "cz", "da", "de", "el",
                                   "en", "eu", "fa", "fi", "fr", "gl", "he", "hi", "hr", "hu",
                                   "id", "it", "ja", "kr", "la", "lt", "mk", "no", "nl", "pl",
                                   "pt", "pt_br", "ro", "ru", "sv", "se", "sk", "sl", "sp", "es",
                                   "sr", "th", "tr", "ua", "uk", "vi", "zh_cn", "zh_tw", "zu"])
        )
        .arg(
            Arg::new("EXCLUDE_CURRENT")
                .long("exclude-current")
                .help("Excludes \"current\" data from the resulting OpenWeather data")
                .required(false)
        )
        .arg(
            Arg::new("EXCLUDE_MINUTELY")
                .long("exclude-minutely")
                .help("Excludes \"minutely\" data from the resulting OpenWeather data")
                .required(false)
        )
        .arg(
            Arg::new("EXCLUDE_HOURLY")
                .long("exclude-hourly")
                .help("Excludes \"hourly\" data from the resulting OpenWeather data")
                .required(false)
        )
        .arg(
            Arg::new("EXCLUDE_DAILY")
                .long("exclude-daily")
                .help("Excludes \"daily\" data from the resulting OpenWeather data")
                .required(false)
        )
        .arg(
            Arg::new("EXCLUDE_ALERTS")
                .long("exclude-alerts")
                .help("Excludes \"alerts\" data from the resulting OpenWeather data")
                .required(false)
        )
        .arg(
            Arg::new("QUERY_RATE")
                .short('r')
                .long("query-rate")
                .help("Rate in seconds to query the OpenWeather \"one call\" API")
                .takes_value(true)
                .required(false)
                .default_value("300")
                .validator(validate_query_rate)
        )
        .arg(
            Arg::new("POSTGRES_URL")
                .short('u')
                .long("postgres-url")
                .help("URL to postgres compatible database")
                .takes_value(true)
                .required(true)
        )
        .arg(
            Arg::new("CA_PATH")
                .long("ca-path")
                .help("Path to private CA root certificate for secure connection to postgres")
                .takes_value(true)
                .required(false)
        )
        .arg(
            Arg::new("DEBUG")
                .long("debug")
                .help("Prints debug messages")
                .required(false)
        )
        .get_matches();

    let is_debug: bool = matches.is_present("DEBUG");

    let url = matches.value_of("POSTGRES_URL").unwrap().to_owned();
    if is_debug {
        println!("Value for POSTGRES_URL: {}", url);
    }

    let latitude_q = matches.value_of("LATITUDE").unwrap();
    let latitude_f = latitude_q.parse::<f64>().unwrap();
    let longitude_q = matches.value_of("LONGITUDE").unwrap();
    let longitude_f = longitude_q.parse::<f64>().unwrap();
    let units = matches.value_of("UNITS").unwrap().to_owned();
    let language = matches.value_of("LANGUAGE").unwrap().to_owned();

    // Build the query string to call the OpenWeather API
    let mut ow_onecall_api = "https://api.openweathermap.org/data/2.5/onecall?".to_owned() +
        "&appid=" + matches.value_of("API_KEY").unwrap() +
        "&lat=" + latitude_q +
        "&lon=" + longitude_q +
        "&units=" + units.as_str() + 
        "&lang=" + language.as_str();

    let exclude_current = matches.is_present("EXCLUDE_CURRENT");
    let exclude_minutely = matches.is_present("EXCLUDE_MINUTELY");
    let exclude_hourly = matches.is_present("EXCLUDE_HOURLY");
    let exclude_daily = matches.is_present("EXCLUDE_DAILY");
    let exclude_alerts = matches.is_present("EXCLUDE_ALERTS");

    if exclude_current || exclude_minutely || exclude_hourly || exclude_daily || exclude_alerts {
        let mut exclude_vec: Vec<&str> = Vec::new();
        if exclude_alerts {
            exclude_vec.push("current");
        }
        if exclude_minutely {
            exclude_vec.push("minutely");
        }
        if exclude_hourly {
            exclude_vec.push("hourly");
        }
        if exclude_daily {
            exclude_vec.push("daily")
        }
        if exclude_alerts {
            exclude_vec.push("alerts");
        }

        let exclude_list = exclude_vec.join(",");

        ow_onecall_api += "&exclude=";
        ow_onecall_api += exclude_list.as_str();
    }
    if is_debug {
        println!("OW API: {}", ow_onecall_api);
    }

    // Make the call to the OpenWeather API at the "query rate" interval
    let r = matches.value_of("QUERY_RATE").unwrap().to_owned();
    let rate: i64 = r.parse::<i64>().unwrap();
    let timer = timer::Timer::new();
    let guard = {
        timer.schedule_repeating(chrono::Duration::seconds(rate), move || {
            // Make the connection to the Postgres database
            let mut client: Client;
            let cr: Result<postgres::Client, postgres::Error>;

            if matches.is_present("CA_PATH") {
                let ca_path = matches.value_of("CA_PATH").unwrap().to_owned();
                if is_debug {
                    println!("CA path: {}", ca_path);
                }

                let mut builder = SslConnector::builder(SslMethod::tls()).unwrap();
                builder.set_ca_file(ca_path).unwrap();
                let connector = MakeTlsConnector::new(builder.build());
                
                cr = Client::connect(url.as_str(), connector);
            } else {
                cr = Client::connect(url.as_str(), NoTls);
            }
            match cr {
                Ok(c) => {
                    client = c;
                    println!("Connection to postgres successful!");

                    println!("Calling OpenWeather API...");

                    // Call the API
                    let res_r = reqwest::blocking::get(&ow_onecall_api);
                    match res_r {
                        Ok(res) => {
                            if res.status() == 200 {
                                let json = &res.text().unwrap();
                                if is_debug {
                                    println!("OW response: {}", json);
                                }
                                let json: serde_json::Value = serde_json::from_str(json).unwrap();

                                // Write data to the database
                                let sql = "INSERT INTO openweather_onecall_data ".to_owned() +
                                    "(latitude, longitude, language_id, unit_id, has_current_data, has_minutely_data, has_hourly_data, has_daily_data, has_alert_data, weather_data) " +
                                    "VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)";
                                
                                let insert_r = client.execute(sql.as_str(),
                                    &[&latitude_f, &longitude_f, &language, &units, &!exclude_current, &!exclude_minutely, &!exclude_hourly, &!exclude_daily, &!exclude_alerts, &json]);

                                match insert_r {
                                    Ok(n) => {
                                        println!("Inserted {} row", n);
                                    }
                                    Err(e) => {
                                        println!("Could not insert row: {}", e);
                                    }
                                }
                            } else {
                                println!("OW response status: {}", res.status());
                            }
                        }
                        Err(e) => {
                            println!("OW API error: {}", e);
                        }
                    }

                    let r = client.close();
                    match r {
                        Ok(()) => {
                            println!("Postgres connection closed");
                        }
                        Err(e) => {
                            println!("Error closing connection to Postgres: {}", e);
                        }
                    }
                }
                Err(e) => {
                    println!("Could not connect to database: {}", e);
                }
            }
            
        })
    };
  
    let running = Arc::new(AtomicBool::new(true));
    let r = running.clone();
    ctrlc::set_handler(move || {
        r.store(false, Ordering::SeqCst);
    })
    .expect("Error setting interrupt handler");
    if is_debug {
        println!("Waiting for interrupt...");
    }
    while running.load(Ordering::SeqCst) {
        thread::sleep(time::Duration::from_millis(10));
    }
    if is_debug {
        println!("Received interrupt! Exiting...");
    }

    // Stop the timer
    drop(guard);
    if is_debug {
        println!("Timer stopped.");
    }
}

fn validate_query_rate(val: &str) -> Result<(), String> {
    let error_msg = "the query rate (in seconds) must be a valid integer value > 0";

    let rate: i32;
    let rate_r = val.parse::<i32>();
    match rate_r {
        Ok(r) => {
            rate = r;
        }
        Err(_e) => {
            return Err(String::from(error_msg));
        }
    }

    if rate <= 0 {
        return Err(String::from(error_msg));
    }

    Ok(())
}

fn validate_lat_long(val: &str) -> Result<(), String> {
    let error_msg = "Latitude/Longitude is not a valid value.";

    let fval: f64;
    let fval_r = val.parse::<f64>();
    match fval_r {
        Ok(f) => {
            fval = f;
        }
        Err(_e) => {
            return Err(String::from("Could not parse Latitude/Longitude value."));
        }
    }

    if fval < -180.0 || fval > 180.0 {
        return Err(String::from(error_msg));
    }
    
    Ok(())
}
